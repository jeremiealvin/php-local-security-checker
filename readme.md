**This is an alternative of outdated [SensioLabs Security Checker](https://github.com/sensiolabs/security-checker)**

# Pull the latest version of [php-local-security-checker](https://github.com/fabpot/local-php-security-checker) and compile it

## [Docker hub](https://hub.docker.com/r/jeremiealvin/php-local-security-checker)

Usage : ```docker run --rm -v $PWD:/code jeremiealvin/php-local-security-checker```
